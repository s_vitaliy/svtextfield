//
//  SVTextField.swift
//  CustomTextField
//
//  Created by V-jet on 29/05/2019.
//  Copyright © 2019 V-jet. All rights reserved.
//

import UIKit

class SVTextField: UIControl {

    private var borderView: UIView?
    private var textField: UITextField?
    private var passwordButton: UIButton?
    private var placeholderLabel: UILabel?
    private var placeholderView: UIView?
    
    private var isShowPassword = false
    
    @IBInspectable
    var insetText: CGFloat = 20
    
    @IBInspectable
    var text: String = "" {
        didSet {
            textField?.text = text
            setupPlaceholderPosition()
        }
    }
    
    @IBInspectable
    var textColor: UIColor = .darkGray {
        didSet {
            textField?.textColor = textColor
        }
    }
    
    @IBInspectable
    var placeholder: String = "" {
        didSet {
            placeholderLabel?.text = placeholder
        }
    }
    
    @IBInspectable
    var placeholderColor: UIColor = .lightGray {
        didSet {
            placeholderLabel?.textColor = placeholderColor
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            borderView?.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            borderView?.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = .clear {
        didSet {
            borderView?.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var borderColorActive: UIColor = .clear
    
    @IBInspectable
    var isPassword: Bool = false {
        didSet {
            textField?.isSecureTextEntry = !isShowPassword
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupBorderView()
        setupTextField()
        setupPlaceholder()
        setupPasswordButton()
    }
    
    override func layoutSubviews() {
        if let labelFrame = placeholderLabel?.frame {
            placeholderView?.frame = CGRect(x: insetText / 2, y: -(labelFrame.height + insetText) / 2, width: labelFrame.width + insetText, height: labelFrame.height + insetText)
        }

        let buttonFrame = passwordButton?.frame ?? .zero
        textField?.frame = CGRect(x: insetText, y: 0, width: bounds.width - insetText * 2 - buttonFrame.width, height: bounds.height)
        
        borderView?.frame = bounds
        
        passwordButton?.frame = CGRect.init(x: bounds.width - 25 - insetText, y: 0, width: 25, height: bounds.height)
    }
}

//MARK: - PASSWORD BUTTON
extension SVTextField {
    
    private func setupPasswordButton() {
        guard isPassword else { return }
        
        let button = UIButton.init(frame: CGRect.init(x: bounds.width - 25 - insetText, y: 0, width: 25, height: bounds.height))
        button.addTarget(self, action: #selector(showPassToggle), for: .touchUpInside)
        button.setImage(isShowPassword ? #imageLiteral(resourceName: "show passwod.pdf") : #imageLiteral(resourceName: "hide password.pdf"), for: .normal)
        addSubview(button)
        passwordButton = button
    }
    
    @objc private func showPassToggle() {
        isShowPassword = !isShowPassword
        passwordButton?.setImage(isShowPassword ? #imageLiteral(resourceName: "show passwod.pdf") : #imageLiteral(resourceName: "hide password.pdf"), for: .normal)
        textField?.isSecureTextEntry = !isShowPassword
    }
}

//MARK: - BORDER VIEW
extension SVTextField {
    
    private func setupBorderView() {
        
        let view = UIView.init(frame: bounds)
        view.layer.cornerRadius = cornerRadius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = borderColor.cgColor
        view.backgroundColor = .clear
        addSubview(view)
        borderView = view
        
        placeholderView = UIView.init(frame: CGRect.zero)
        placeholderView?.backgroundColor = .white
        placeholderView?.isUserInteractionEnabled = false
        placeholderView?.alpha = 0
        
        addSubview(placeholderView!)
    }
}

//MARK: - TEXTFIELD
extension SVTextField {
    
    private func setupTextField() {
        
        textField = UITextField.init(frame: bounds.inset(by: UIEdgeInsets(top: 0, left: insetText, bottom: 0, right: insetText)))
        textField?.delegate = self
        textField?.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        textField?.textColor = textColor
        textField?.font = UIFont(name: "K2D-Regular", size: 16)
        textField?.isSecureTextEntry = !isShowPassword && isPassword
        addSubview(textField!)
    }
}

//MARK: - PLACEHOLDER
extension SVTextField {
    
    private func setupPlaceholder() {
        
        guard !placeholder.isEmpty else {
            
            placeholderLabel?.removeFromSuperview()
            return
        }
        
        placeholderLabel = UILabel.init()
        placeholderLabel?.text = placeholder
        placeholderLabel?.textColor = placeholderColor
        placeholderLabel?.font = UIFont(name: "K2D-Regular", size: 16)
        placeholderLabel?.sizeToFit()
        let currentFrame = placeholderLabel?.frame ?? .zero
        placeholderLabel?.frame = CGRect.init(x: insetText, y: bounds.height / 2 - currentFrame.height / 2, width: currentFrame.width, height: currentFrame.height)
        
        addSubview(placeholderLabel!)
    }
    
    private func setupPlaceholderPosition() {
        guard let label = placeholderLabel else { return }
        
        var transform = CGAffineTransform.identity
        var alpha: CGFloat = 0

        if !text.isEmpty || textField?.isFirstResponder == true {
            
            transform = CGAffineTransform(scaleX: 0.8, y: 0.8).concatenating(CGAffineTransform(translationX: -(label.bounds.width - label.bounds.width * 0.8) / 2, y: -(bounds.height / 2)))
            alpha = 1
        }
        
        UIView.animate(withDuration: 0.3) {
            label.transform = transform
            self.borderView?.layer.borderColor = self.textField?.isFirstResponder == true ? self.borderColorActive.cgColor : self.borderColor.cgColor
            self.placeholderView?.alpha = alpha
        }
    }
}

//MARK: - TEXTFIELD DELEGATE
extension SVTextField: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        setupPlaceholderPosition()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        setupPlaceholderPosition()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc private func textChanged(_ sender: UITextField) {
        text = sender.text ?? ""
    }
}
